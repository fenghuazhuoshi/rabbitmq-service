package cn.jwis;

import cn.jwis.configration.clientUtil.ConfigCenterHelper;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan( basePackages = { "cn.jwis" } )
@EnableRabbit
@EnableSwagger2
public class FoundationApplication {

    public static void main(String[] args)throws Exception {
        SpringApplication springApplication = new SpringApplication(FoundationApplication.class);
        springApplication.run(args);

    }

}
