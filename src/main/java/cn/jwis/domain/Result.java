package cn.jwis.domain;

public class Result {
	private int code;
	private Object result;
	// 如果有错误的时候，这个字段才会有值
	private String msg;

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Result() {
	}

	public Result(int code, Object result) {
		this.code = code;
		this.result = result;
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	// 成功
	public static Result success(Object result) {
		Result ret = new Result(0, result);
		return ret;
	}

	public static Result pagination(int count, int currentPage, int pageSize, Object rows) {
		PageResult page = new PageResult(count,currentPage,pageSize, rows);
		Result ret = new Result(0, page);
		return ret;
	}

	public static Result success() {
		Result ret = new Result(0, null);
		return ret;
	}

	// 失败
	public static Result error(int code, String msg) {
		Result ret = new Result(code, msg);
		return ret;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}

class PageResult {
	private Object rows;
	private int count;
	private int currentPage;
	private int pageSize;
	public PageResult(int count,int currentPage,int pageSize, Object rows) {
		this.count = count;
		this.rows = rows;
		this.currentPage = currentPage;
		this.pageSize = pageSize;
	}

	public Object getRows() {
		return rows;
	}

	public void setRows(Object rows) {
		this.rows = rows;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
}