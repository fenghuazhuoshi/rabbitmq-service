package cn.jwis.domain;
import org.springframework.stereotype.Component;

/**
 * @author :王泽华
 * @Title: MessageVo
 * @ProjectName mq-message
 * @Description: TODO
 * @date 2019/8/618:36
 */
@Component
public class MessageVo {

    private String queueName;
    private String data;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
