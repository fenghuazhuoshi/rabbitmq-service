package cn.jwis.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
public class SwaggerConfig {
	public static final Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);

/*	@Bean
	public Docket productApi() {
		// 添加head参数appId
		List<Parameter> pars = new ArrayList<Parameter>();
		pars = addHeaderParam("appId", "appId", pars, true);
		pars = addHeaderParam("accesstoken", "accesstoken", pars, false);

		Properties properties = new Properties();
		try {
			InputStream is = SwaggerConfig.class.getClassLoader().getResourceAsStream("appName.properties");
			if (is != null) {
				properties.load(is);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		String packageName = properties.getProperty("swagger.package");
		return new Docket(DocumentationType.SWAGGER_2).genericModelSubstitutes(DeferredResult.class)
				.useDefaultResponseMessages(false).forCodeGeneration(true).select()
				.apis(RequestHandlerSelectors
						.basePackage((packageName != null && !packageName.trim().equals("")) ? packageName : "cn.jwis"))
				.paths(PathSelectors.regex("/.*"))// 过滤的接口
				.build().globalOperationParameters(pars).apiInfo(metaData());

	}

	private List<Parameter> addHeaderParam(String name, String description, List<Parameter> pars, boolean flag) {
		ParameterBuilder header = new ParameterBuilder();
		header.name(name).description(description).modelRef(new ModelRef("string")).parameterType("header")
				.required(flag).build();
		pars.add(header.build());
		return pars;
	}

	private ApiInfo metaData() {
		ApiInfo apiInfo = new ApiInfo("REST APIs", "Provide the APIs manual for JWI product", "1.0", "Terms of service",
				new Contact("JWI Team", "http://www.jwis.cn", "samuelluo@jwis.cn"), "", "");
		return apiInfo;
	}*/
	@Bean
	public Docket accessToken() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("api")// 定义组
				.select() // 选择那些路径和 api 会生成 document
				.apis(RequestHandlerSelectors.basePackage("cn.jwis")) // 拦截的包路径
				.paths(regex("/.*"))// 拦截的接口路径
				.build() // 创建
				.apiInfo(apiInfo()); // 配置说明
	}
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()//
				.title("rabbit mq ")// 标题
				.description("rabbit mq")// 描述
				//.termsOfServiceUrl("http://www.roncoo.com")//
				//.contact(new Contact("wujing", "http://www.roncoo.com",
				//		"297115770@qq.com"))// 联系
				// .license("Apache License Version 2.0")// 开源协议
				// .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")// 地址
				.version("1.0")// 版本
				.build();
	}

}