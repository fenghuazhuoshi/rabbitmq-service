package cn.jwis.service.interf;

import cn.jwis.domain.MessageVo;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author :王泽华
 * @Title: MessageService
 * @ProjectName mq-message
 * @Description: TODO
 * @date 2019/8/513:59
 */
public interface MessageService {

    public void  sendMessage(MessageVo message)throws Exception;

    public void createTopic(String topicName)throws Exception;

    public void dynamicCreateConsumer(String topicName)throws Exception;

    public void stopConsumer(String topicName)throws Exception;

    public void sendFileMessage(String queueName, MultipartFile file);
}
