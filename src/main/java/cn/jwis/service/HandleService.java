package cn.jwis.service;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @Author 王泽华
 * @Description
 * @Date 15:51 2019/6/20
 * @Param
 * @return
 */
@Service
public class HandleService implements ChannelAwareMessageListener {
    private static final Logger logger = LoggerFactory.getLogger(HandleService.class);


     /**
     * @param 
     * 1、处理成功，这种时候用basicAck确认消息；
     * 2、可重试的处理失败，这时候用basicNack将消息重新入列；
     * 3、不可重试的处理失败，这时候使用basicNack将消息丢弃。
     * 
     *  basicNack(long deliveryTag, boolean multiple, boolean requeue)
     *   deliveryTag:该消息的index
      *  multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
       * requeue：被拒绝的是否重新入队列
     */
     @Override
     public void onMessage(Message message, Channel channel) throws IOException {
         channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);//确认消息消费成功 
         byte[] body = message.getBody();
         logger.info("接收到消息:" + new String(body));
         JSONObject chatContent = JSONObject.parseObject(new String(body));
         String toUserOid=chatContent.getString("receiveOid");
        /* List<WebSocketController> webSocketControllers = WebSocketController.getWebsocketPool().get(toUserOid);
         if(webSocketControllers!=null&&webSocketControllers.size()>0){
             for(WebSocketController webSocketController:webSocketControllers){

                 webSocketController.getSession().getBasicRemote().sendText( new String(body));
             }
         }*/
     }
    }