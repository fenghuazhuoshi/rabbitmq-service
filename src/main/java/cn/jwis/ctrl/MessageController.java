package cn.jwis.ctrl;
import cn.jwis.domain.MessageVo;
import cn.jwis.domain.Result;
import cn.jwis.service.interf.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/message")
@Api(tags = "Pub/Sub message", value = "Pub/Sub message", description = "Pub/Sub message")
public class MessageController
{

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/createTopic", method = RequestMethod.GET)
    @ApiOperation(response = Result.class, value = "创建topic(对列)", notes = "创建topic（对列）")
    public Result createTopic(
            @RequestParam("topicName") String queueName)throws  Exception {
        Result responseEntity = new Result();
        messageService.createTopic(queueName);
        return responseEntity;

    }

    @RequestMapping(value = "/createConsumer", method = RequestMethod.GET)
    @ApiOperation(response = Result.class, value = "消费队列", notes = "消费对列")
    public Result dynamicCreateConsumer(
            @RequestParam("topicName") String queueName)throws  Exception {
        Result responseEntity = new Result();
        messageService.dynamicCreateConsumer(queueName);
        return responseEntity;

    }

    @RequestMapping(value = "/stopConsumer", method = RequestMethod.GET)
    @ApiOperation(response = Result.class, value = "消费者名称和队列名称一样", notes = "暂停消费者")
    public Result stopConsumer(
            @RequestParam("topicName") String queueName)throws  Exception {
        Result responseEntity = new Result();
        messageService.dynamicCreateConsumer(queueName);
        return responseEntity;

    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    @ApiOperation(response = Result.class, value = "send message", notes = "POST模式发送message到指定队列")
    public Result sendMessage(@RequestBody MessageVo message) throws Exception{
        Result responseEntity = new Result();
        messageService.sendMessage(message);
        return responseEntity;

    }

    @RequestMapping(value = "/sendFileMessage", method = RequestMethod.GET)
    @ApiOperation(response = Result.class, value = "send message", notes = "GET模式发送message到指定队列")
    public Result sendFileMessage(
             @RequestParam("queueName") String queueName,
            @RequestParam("file") MultipartFile file) {
        Result responseEntity = new Result();
        messageService.sendFileMessage(queueName,file);
        return responseEntity;

    }



}
