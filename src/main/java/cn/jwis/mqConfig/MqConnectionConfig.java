package cn.jwis.mqConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MqConnectionConfig {

	@Value("${spring.rabbitmq.port}")
	int port;
	@Value("${spring.rabbitmq.host}")
	String host;
	@Value("${spring.rabbitmq.username}")
	String userName;
	@Value("${spring.rabbitmq.password}")
	String password;
	@Value("${spring.rabbitmq.virtual-host}")
	String vhost;
	@Value("${spring.rabbitmq.exchange}")
	String exchange;

	private CachingConnectionFactory connectionFactory;

	private  RabbitTemplate rabbitTemplate;
	private  RabbitAdmin rabbitAdmin;

	private static final Logger logger = LoggerFactory.getLogger(MqConnectionConfig.class);

	public  void closeConnection(CachingConnectionFactory connectionFactory) {
		try {
			connectionFactory.destroy();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	public ConnectionFactory getConnectionFactory() {
		if (connectionFactory == null) {
			connectionFactory = new CachingConnectionFactory();
			//connectionFactory.setAddresses(address);
			connectionFactory.setPort(port);
			connectionFactory.setHost(host);
			connectionFactory.setUsername(userName);
			connectionFactory.setPassword(password);
			connectionFactory.setPublisherConfirms(true);
			logger.info("连接部署测试MQ:" + userName + ":" + password + "@" + host );
		}
		return connectionFactory;
	}


	@Bean
	public  RabbitTemplate getRabbitTemplate() throws Exception {
		if (rabbitTemplate == null) {
			rabbitTemplate = new RabbitTemplate(getConnectionFactory());
			rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
		}
		//rabbitTemplate.receive();
		//rabbitTemplate.convertAndSend();
		return rabbitTemplate;
	}

	@Bean
	public  RabbitAdmin getRabbitAdmin() throws Exception{
		if (rabbitAdmin == null) {
			rabbitAdmin = new RabbitAdmin(getConnectionFactory());
		}
		return rabbitAdmin;
	}
	@Bean
	public AmqpProducer amqpProducer(ConnectionFactory connectionFactory) {
		return new AmqpProducer(connectionFactory);
	}

	public class AmqpProducer {

		private AmqpTemplate amqpTemplate;

		public AmqpProducer(ConnectionFactory connectionFactory) {
			amqpTemplate = new RabbitTemplate(connectionFactory);
		}

		/**
		 * 将消息发送到指定的交换器上
		 *
		 * @param exchange
		 * @param msg
		 */
		public void publishMsg(String exchange, String routingKey, Object msg) {
			amqpTemplate.convertAndSend(exchange, routingKey, msg);
		}
	}




}
