package cn.jwis.mqConfig;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;

public class DynamicConsumer {
 
 
    private static final Logger logger = LoggerFactory.getLogger(DynamicConsumer.class);
    private SimpleMessageListenerContainer container;
 
    public DynamicConsumer(MQContainerFactory fac) throws Exception {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(fac.getConnectionFactory());
        container.setQueueNames(fac.getQueue());
        container.setMessageListener(new MqConsumer() {
            public boolean process(Message message, Channel channel) {
                distributionConsumerMsg(message, channel);
                return true;
            }
        });
        this.container = container;
    }
 
    //启动消费者监听
    public void start() {
        container.start();
    }
 
   //消费者停止监听
    public void stop() {
        container.stop();
    }
 
    //消费者重启
    public void shutdown() {
        container.shutdown();
    }
 
 
    /**
     * 用户扩展处理消息
     */
    public void distributionConsumerMsg(Message message, Channel channel) {
        byte[] body = message.getBody();
        System.out.println("处理数据："+new String(body));

    }
 
}
